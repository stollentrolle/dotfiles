﻿;; just in case recompile all the emacs lisp code on startup
;; Quelle: https://stackoverflow.com/questions/1217180/how-do-i-byte-compile-everything-in-my-emacs-d-directory
;; Wenn der Start mit einer unverständlichen Meldung scheitert,
;; dann kann man alle vorkompilierten Dateien entfernen und sie werden
;; beim nächsten Start neu erzeugt.
;; --> symbol's function definition is void: make-closure
;;     find ~/.emacs.d -type f -name \*elc -exec rm {} \;
;;     find ~/.emacs.d -type f -name \*eln -exec rm {} \;
;;(byte-recompile-directory (expand-file-name "~/.emacs.d") 0)
(setq frame-background-mode 'dark)

;;
;; MELPA
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; marmalade is down since 2021
;;(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/") t)
(package-initialize)
(setq package-install-upgrade-built-in t)

;;(package-install 'use-package)
(require 'use-package)

(setq default-major-mode 'text-mode)

;; icomplete or fido-mode enable an easier way to switch buffers
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
(setq fido-mode 't)

(add-to-list `auto-mode-alist '("\\.svg\\'" . xml-mode))

;; ebook reading with nov-mode
(add-to-list `auto-mode-alist '("\\.epub\\'" . nov-mode))
(setq nov-text-width 80)

;; Autofill
(setq text-mode-hook 'turn-on-auto-fill)
(setq org-mode-hook 'turn-on-auto-fill)
(setq markdown-mode-hook 'turn-on-auto-fill)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(eglot eldoc erc faceup flymake idlwave jsonrpc project soap-client tramp verilog-mode xref php-mode rainbow-identifiers yaml rainbow-delimiters go-mode graphviz-dot-mode nov yaml-mode gnuplot-mode org-roam bibtex-utils json-reformat json-mode markdown-mode lua-mode transient-cycles compat org magit htmlize f use-package)))
   ;;            '(lua-mode transient-cycles compat org magit htmlize f emacsql-sqlite3 emacsql-sqlite use-package)))
   ;; '(package-selected-packages '(org-roam markdown-mode magit json-reformat json-mode)))

;; magit sollte >= 3.0 sein
;; (add-to-list 'load-path "~/sources/Emacs/magit/lisp")
(require 'magit)

;; rainbow-delimiters for all programming languages
;;(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
;;(add-hook 'org-mode-hook #'rainbow-delimiters-mode)
;;(add-hook 'markdown-mode-hook #'rainbow-delimiters-mode)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(rainbow-delimiters-depth-1-face ((t (:foreground "dark orange"))))
 '(rainbow-delimiters-depth-2-face ((t (:foreground "deep pink"))))
 '(rainbow-delimiters-depth-3-face ((t (:foreground "chartreuse"))))
 '(rainbow-delimiters-depth-4-face ((t (:foreground "deep sky blue"))))
 '(rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
 '(rainbow-delimiters-depth-6-face ((t (:foreground "orchid"))))
 '(rainbow-delimiters-depth-7-face ((t (:foreground "spring green"))))
 '(rainbow-delimiters-depth-8-face ((t (:foreground "sienna1")))))

(with-eval-after-load 'info
 (info-initialize)
 (add-to-list 'Info-directory-list
              "~/.emacs.d/site-lisp/magit/Documentation/"))

;; Welches LISP verwendet SLIME:
(setq inferior-lisp-program "~/bin/sbcl")


;; org-roam Version 2.2.2 statt bei Debian 1.2.3
;;(add-to-list 'load-path "/home/roland/sources/Emacs/org-roam")

(require 'org-roam)
;;(setq org-roam-database-connector 'emacsql)
;;(setq org-roam-database-connector 'sqlite-builtin)

;; Org-Roam basic configuration
(setq org-directory (concat (getenv "HOME") "/Documents/Org-Roam"))

(use-package org-roam
  :after org
  :init (setq org-roam-v2-ack t) ;; Acknowledge V2 upgrade
  :custom
  (org-roam-directory (file-truename org-directory))
  :config
  (org-roam-setup)
  :bind (("C-c n f" . org-roam-node-find)
	 ("C-c n r" . org-roam-node-random)
	 (:map org-mode-map
       (("C-c n i" . org-roam-node-insert)
		("C-c n o" . org-id-get-create)
		("C-c C-o" . org-open-at-point)
		("C-c n t" . org-roam-tag-add)
		("C-c n a" . org-roam-alias-add)
		("C-c n l" . org-roam-buffer-toggle)))))

(org-roam-db-autosync-mode)

;; ;;
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )

;; immer mit SPACE einrücken, nie mit TAB
(setq-default indent-tabs-mode nil)

;; Zeilennummer absolut
(require 'display-line-numbers)

;; (defcustom display-line-numbers-exempt-modes
;;   '(vterm-mode eshell-mode shell-mode term-mode ansi-term-mode)
;;   "Major modes on which to disable line numbers."
;;   :group 'display-line-numbers
;;   :type 'list
;;   :version "green")

;;(defun display-line-numbers--turn-on ()
;;  "Turn on line numbers except for certain major modes.
;;Exempt major modes are defined in `display-line-numbers-exempt-modes'."
;;  (unless (or (minibufferp)
;;              (member major-mode display-line-numbers-exempt-modes))
;;    (display-line-numbers-mode)))

(global-display-line-numbers-mode)

;; ;; relative Zeilennummer (wie weit muss ich springen) sind so möglich
;; ;; https://kevin-mandura.de/tutorials/emacs/zeilennummerierung-aktivieren.html
;;                                         ;
;; ;; Specify type for displaying line numbers
;; ;; (setq display-line-numbers-type 'relative)

;; ;; ;; Enable display of relative line numbers
;; ;; ;; excepted for certain modes
;; ;; (global-display-line-numbers-mode 1)
;; ;; (dolist (mode '(org-mode-hook
;; ;;               term-mode-hook
;; ;;               eshell-mode-hook
;; ;;               shell-mode-hook))
;; ;;   (add-hook mode (lambda () (display-line-numbers-mode -1))))

(setq column-number-mode t)

;; Links auf Seite in PDF
;; https://emacs.stackexchange.com/questions/30344/how-to-link-and-open-a-pdf-file-to-a-specific-page-skim-adobe
(org-add-link-type "pdf" 'org-pdf-open nil)

(defun org-pdf-open (link)
  "Where page number is 105, the link should look like:
   [[pdf:/path/to/file.pdf#page=105][My description.]]"
  (let* ((path+page (split-string link "#page="))
         (pdf-file (car path+page))
         (page (car (cdr path+page))))
    (start-process "view-pdf" nil "evince" "--page-index" page pdf-file)))

;; Code in Org-Mode-Dateien ausführbar machen
;; Dies passiert auch beim Export (wenn alles richtig konfiguriert ist)
;; Quellen:
;; https://www.johndcook.com/blog/2022/08/02/org-babel-vs-jupyter/
;; https://sqrtminusone.xyz/posts/2021-05-01-org-python/
;; noch ungelesen: https://michaelneuper.com/posts/replace-jupyter-notebook-with-emacs-org-mode/
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   ;; Other languages
   (shell . t)
   ;; Python & Jupyter
   (python . t)
   ;;
   ;;(jupyter . t)
   ;;(R . t)
   (perl . t)
   ;; ditaa
   (ditaa . t)
   ))

;; ditaa für schöne Bilder aus ASCII-Art
;; Set the path to the Ditaa JAR file
(setq org-ditaa-jar-path "/usr/share/java/ditaa/ditaa.jar")

(load-file (let ((coding-system-for-read 'utf-8))
                (shell-command-to-string "agda-mode locate")))

(add-hook 'before-save-hook
          'delete-trailing-whitespace)
;;
;; opam / OCaml
(add-to-list 'load-path "/home/rh/.opam/default/share/emacs/site-lisp")
(require 'ocp-indent)

(load "~/.emacs.d/site-lisp/yaml-mode.el")
(add-hook 'yaml-mode-hook
          (lambda ()
            (define-key yaml-mode-map "\C-m" 'newline-and-indent)))
