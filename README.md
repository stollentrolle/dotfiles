# My personal dotfiles

Regardless of the billions of dotfiles in my $HOME only a few are important to me and my daily workflow.

And even these few are only crafted marginally to fit my taste and workflow, even after decades of using Linux and BSD Unix.
