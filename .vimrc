" .vimrc of RH
syntax on
set bg=dark
"set mouse=nicr
set mouse-=a
set number
"
setl ff=unix nofixeol
"
"set textwidth=72
set autoindent
set fileformat=unix
set hlsearch
" don't indent Makefile with SPACE
autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=0
"
" see https://forums.freebsd.org/threads/vim.27993/
set nocompatible        " Use Vim settings, rather then Vi settings
set backspace=indent,eol,start  " allow backspacing over everything
set ruler               " show the cursor position all the time
set incsearch           " do incremental searching
set hlsearch            " highlight the last used search pattern.
set autoindent          " always set auto indenting on
set lbr                 " Wrap at word
set tabstop=4           " Tabs are 4 spaces wide
set expandtab
set shiftwidth=4        " Auto-indent 4 spaces wide
set softtabstop=4       " Still 4...
set encoding=utf-8      " Default encoding
"
" YAML
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
let g:indentLine_char = '¦'
"
" unfold all
au! BufNewFile,BufReadPost *.{yaml,yml} set filetype=yaml foldmethod=indent
set foldlevel=99
"
" Enable folding
"set foldmethod=indent
"set foldlevel=99
"
" opam / OCaml
set rtp^="/home/rh/.opam/default/share/ocp-indent/vim"

