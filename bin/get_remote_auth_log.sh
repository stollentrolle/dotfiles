#!/bin/bash
#
scp root@vHost-Neu:/var/log/auth.log ~/Documents/Computer/Zugriffe-vHost/auth.log-$(date +%Y%m%d-%H%M)
scp root@vHost-Neu:/var/log/fail2ban.log ~/Documents/Computer/Zugriffe-vHost/fail2ban.log-$(date +%Y%m%d-%H%M)
scp root@vHost-Neu:/var/lib/fail2ban/fail2ban.sqlite3 ~/Documents/Computer/Zugriffe-vHost/fail2ban-$(date +%Y%m%d-%H%M).sqlite3

