
# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# das leidige Problem mit den vielfältigen Shells und
# nur einer History lösen
# Die folgenden Einstellung bewirken das Schreiben der
# History bei jedem <ENTER>
# Aus: Liebel, Ungar: OpenLDAP 2.4 - Das Praxisbuch, Seite 71
PROMPT_COMMAND="history -a"
shopt -s histappend
#
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=100000
HISTFILESIZE=2000000
HISTTIMEFORMAT='%F %T '
# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# Diese Variable wird im default-Prompt verwendet - ist hier aber ungenutzt
# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    linux|xterm|xterm-ghostty|xterm-color|*-256color) color_prompt=yes;;
esac

# 20.02.2021 - Das default Gnome von Debian kann das Touchpad deaktivieren :-)
# LXDE hat keine Option für das Touchpad, aber man kann es deaktivieren
# https://wiki.archlinux.org/index.php/Touchpad_Synaptics
# Anmerkung: Die if[]-Abfrage erzeugt eine Datei namens '0' ...
# if [ $(echo $TERM | grep -c xterm > /dev/null 2>&1) > 0 ]; then
#  echo "Yo. Schalte Touchpad *aus*. ";
#/usr/bin/synclient TouchpadOff=1
# fi

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

# Putty kann Farbe
if [ "$COLORTERM" == "1" ]; then
	color_prompt=yes
fi

# Dem Admin sein Zuhause kann es mit Putty angeblich nicht ..."
if [ "$HOSTNAME" == "WSRH2" ]; then
	color_prompt=yes
fi

MY_OS=$(~/bin/which_os.sh)
if [ "$color_prompt" = yes ]; then
  if [ $USER = 'root' ]; then
    PS1='\[\033[01;35m\]${MY_OS}\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
  else
    PS1='\[\033[01;35m\]${MY_OS}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\W\[\033[00m\]\$ '
  fi
else
    PS1='\u@\h:\W\$ '
fi
unset color_prompt force_color_prompt


# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias cd3="cd ../../.."
alias cd2="cd ../.."
alias cmake-debug='cmake -DCMAKE_BUILD_TYPE=Debug'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
#alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

alias lua=lua5.3
alias python=python3

alias vil='vim -u ~/.vimrc_light'
alias vid='vim -u ~/.vimrc_dark'

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

#
# If this is an xterm disable all screensavers
# Sadly, PuTTY claims to be an xterm :-/
if [ "x$DISPLAY" == "x" ]; then
    echo Kein X-Window Display gesetzt.
else
    if [ -x /usr/bin/xset ]; then
        case "$TERM" in
            xterm*|rxvt*)
                xset s noblank
                xset s off
                xset -dpms
                ;;
            *)
                ;;
        esac
    fi
    # CAPS LOCK deaktivieren
    if [ -x /usr/bin/setxkbmap ]; then
        setxkbmap -option ctrl:nocaps
    fi
    xset s off
    xset -dpms

fi


## enable programmable completion features (you don't need to enable
## this, if it's already enabled in /etc/bash.bashrc and /etc/profile
## sources /etc/bash.bashrc).
#if ! shopt -oq posix; then
#  if [ -f /usr/share/bash-completion/bash_completion ]; then
#    . /usr/share/bash-completion/bash_completion
#  elif [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#  fi
#fi

export PATH=/opt/homebrew/bin:~/.cabal/bin:~/racket/bin:/usr/local/texlive/2019/bin/x86_64-linux/:${PATH}:
export EDITOR=vim
export GIT_EDITOR=vim

# set PATH so it includes user's private bin if it exists
# Die Prüfung ob ~/bin existiert ist verloren gegangen
# Ebenso ist ${LPATH} nicht mehr gesetzt
PATH="$HOME/bin:${PATH}:/sbin:/usr/sbin:/usr/local/sbin:"
export PATH

# GOlang
export GOROOT=/usr/lib/go
export PATH=$GOROOT/bin:$PATH
export GOPATH=~/go
#
export DENO_INSTALL="~/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"
export KITTY_CONFIG_DIRECTORY=~/.config/kitty

echo Informationen vom vHost-Neu abholen?
echo ToDo: Abfrage einbauen, Default=Nein  
#if [ $HOSTNAME == 'e580' ] ; then
#    echo "Hole Informationen vom vHost-Neu ab."
#    bin/get_remote_auth_log.sh
#fi

export PICO_SDK_PATH=/home/roland/pico/pico-sdk
export PICO_EXAMPLES_PATH=/home/roland/pico/pico-examples
export PICO_EXTRAS_PATH=/home/roland/pico/pico-extras
export PICO_PLAYGROUND_PATH=/home/roland/pico/pico-playground

# OCaml 
# Alternativ kann man vor jeder Verwendung auch
#eval $(opam env)
# oder
#eval $(opam env --switch=default)
# ausführen. Letzteres ist für das Default-Environment, wenn man mehrere eingerichtet hat.
test -r '/home/rh/.opam/opam-init/init.sh' && . '/home/rh/.opam/opam-init/init.sh' > /dev/null 2> /dev/null || true

# --- Ende ---

